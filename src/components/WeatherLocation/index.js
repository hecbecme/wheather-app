import React, {Component} from 'react';
import CircularProgress from '@material-ui/core/CircularProgress';
import PropTypes from 'prop-types';
import getURLWeatherByCity from './../../services/getURLWeatherByCity';
import TransformWeather from './../../services/transformWeather';
import Location from './Location';
import WeatherData from './WeatherData';
import './styles.css';

class WeatherLocation extends Component {
    
    constructor(props) {
        super(props);
        const { city } = props;

        this.state = {
            city,
            data: null
        }
        // console.log("Constructor");
    }

    componentDidMount(){
        // console.log("componentDidMount");
        this.handleUpdateClick();
    }
    
    componentDidUpdate(prevProps, prevState) {
        // console.log("componentDidUpdate");      
    }
    
    // componentWillMount() {
    //     console.log("UNSAFE componentWillMount");
    // }
    
    // componentWillUpdate(){
    //     console.log("UNSAFE componentWillUpdate");
    // }
    
    handleUpdateClick = () => {
        const api_weather = getURLWeatherByCity( this.state.city )
        fetch(api_weather).then( resolve => {
            
            return resolve.json();
        }).then( data => {
            // console.log("Resultado del handleUpdateClick")
            const newWeather = TransformWeather(data);
            this.setState({
                data: newWeather
            })
        });
    }

    render = () => {
        const { onWeatherLocationClick } = this.props;
        const { city, data } = this.state;
        return(
            <div 
                className = "WeatherLocationCont" 
                onClick = { onWeatherLocationClick }>
                
                <Location 
                    city={ city } />
                { 
                    data  ? <WeatherData data={ data } />
                         : <CircularProgress size={50} />
                }
            </div>
        );
    }
}

WeatherLocation.propTypes = {
    city: PropTypes.string.isRequired,
    onWeatherLocationClick: PropTypes.func,
}

export default WeatherLocation;